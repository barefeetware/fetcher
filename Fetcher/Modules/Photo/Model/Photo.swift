//
//  Photo.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation

struct Photo {
    let id: String
    let owner: String
    let secret: String
    let server: String
    let farm: Int
    let title: String
    // TODO: Convert these Int 0/1 values to Bool true/false
    let isPublic: Int
    let isFriend: Int
    let isFamily: Int
}

extension Photo: Identifiable {}

extension Photo: Decodable {
    enum CodingKeys: String, CodingKey {
        case id
        case owner
        case secret
        case server
        case farm
        case title
        case isPublic = "ispublic"
        case isFriend = "isfriend"
        case isFamily = "isfamily"
    }
}

extension Photo {
    var url: URL? {
        URL(string: "https://farm\(farm).static.flickr.com/\(server)/\(id)_\(secret).jpg")
    }
}
