//
//  PhotoView.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import SwiftUI

struct PhotoView {
    let image: Image
    let text: Text?
}

extension PhotoView: View {
    var body: some View {
        ScrollView {
            VStack {
                text
                    .padding()
                image
                    .resizable(resizingMode: .stretch)
                    .aspectRatio(contentMode: .fit)
            }
        }
    }
}

struct PhotoView_Previews: PreviewProvider {
    static var previews: some View {
        PhotoView(
            image: Image(systemName: "photo"),
            text: Text("Photo")
        )
    }
}
