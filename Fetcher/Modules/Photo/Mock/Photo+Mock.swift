//
//  Photo+Mock.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation

extension Photo {
    static let basic = Photo(
        id: "1234",
        owner: "someone",
        secret: "secret1234",
        server: "server1234",
        farm: 4567,
        title: "photo title 9876",
        isPublic: 1,
        isFriend: 1,
        isFamily: 1
    )
}
