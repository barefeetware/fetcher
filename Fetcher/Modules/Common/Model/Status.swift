//
//  Status.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation

enum Status<Success, Failure: Error> {
    case success(Success)
    case failure(Failure)
    case inProgress
    
    init(result: Result<Success, Failure>) {
        switch result {
        case .success(let success):
            self = .success(success)
        case .failure(let failure):
            self = .failure(failure)
        }
    }
}
