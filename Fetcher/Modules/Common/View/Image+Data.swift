//
//  Image+Data.swift
//  Flickr
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import SwiftUI

extension Image {
    init(data: Data) {
        guard let uiImage = UIImage(data: data)
        else {
            self = Image(systemName: "photo")
            return
        }
        self = Image(uiImage: uiImage)
    }
}
