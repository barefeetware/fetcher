//
//  Flickr.PhotosResponse.Page+Mock.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation

extension Flickr.PhotosResponse.Page {
    static let basic = Self.init(
        page: 3,
        pageCount: 45,
        perPageCount: 67,
        total: "890",
        photos: [.basic]
    )
}
