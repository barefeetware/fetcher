//
//  SearchScene.ViewModel.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation
import Combine
import BFWFetch
import SwiftUI // Only for Image type, stored in photoStatusDict

extension SearchScene {
    class ViewModel: ObservableObject {
        let title = "Flickr"
        let searchPlaceholder = "Search"
        @Published var filter = ""
        @Published var sections: [Section] = []
        /// Dictionary to store fetched images.
        @Published var photoStatusDict: [String: Status<Image, Error>] = [:]
        private let textUpdateDelay: DispatchQueue.SchedulerTimeType.Stride = .seconds(0.3)
        private var subscribers = Set<AnyCancellable>()
    }
}

extension SearchScene.ViewModel {
    
    struct Grid {
        static let spacing: CGFloat = 4.0
        static let minimum: CGFloat = 100.0
        static let maximum: CGFloat = 200.0
    }
    
    enum Symbol: String {
        case error = "exclamationmark.icloud"
    }
    
    struct Section: Identifiable {
        let page: Int
        let photosResult: Result<Flickr.PhotosResponse, Error>?
        
        var id: Int { page }
    }
    
}

extension SearchScene.ViewModel {
    
    func onAppear() {
        if subscribers.isEmpty {
            subscribe()
        }
    }
    
    func onAppear(photo: Photo) {
        fetchImage(photo: photo)
    }
    
    func onAppearNextPageLoadingView() {
        fetchNextPage()
    }
    
    func status(photo: Photo) -> Status<Image, Error>? {
        photoStatusDict[photo.id]
    }
    
    var nextPage: Int {
        (sections.last?.page ?? 0) + 1
    }
    
    var isVisibleNextPageLoadingView: Bool {
        canFetchNextPage
    }
    
    var loadingMessage: String {
        "Loading page \(nextPage)"
    }
    
}

private extension SearchScene.ViewModel {
    
    func subscribe() {
        $filter
            .debounce(for: textUpdateDelay, scheduler: DispatchQueue.main)
            .flatMap {
                Flickr.Rest.searchPublisher(filter: $0)
            }
            .result()
            .map { [Section(page: 1, photosResult: $0)] }
            .receive(on: DispatchQueue.main)
            .sink { self.sections = $0 }
            .store(in: &subscribers)
    }
    
    func shouldFetch<Success, Failure>(
        status: Status<Success, Failure>?
    ) -> Bool {
        switch status {
        case .none: return true
        case .inProgress, .success(_), .failure(_): return false
        }
    }
    
    // TODO: caching strategy to free up memory from old loaded photo images.
    
    func fetchImage(photo: Photo) {
        let status = photoStatusDict[photo.id]
        guard shouldFetch(status: status) else { return }
        guard let url = photo.url else {
            // TODO: Send an error in the publisher.
            return
        }
        photoStatusDict[photo.id] = .inProgress
        URLRequest(url: url)
            .dataPublisher()
            .map { Image(data: $0) }
            .result()
            .map { Status(result: $0) }
            .receive(on: DispatchQueue.main)
            .sink { self.photoStatusDict[photo.id] = $0 }
            .store(in: &subscribers)
    }
    
    var canFetchNextPage: Bool {
        // TODO: Simplify logic
        guard let lastSection = sections.last,
              let lastPhotosResult = try? lastSection.photosResult?.get()
        else { return false }
        return lastSection.page > 0 && lastPhotosResult.page.page < lastPhotosResult.page.pageCount
            && !photoStatusDict.isEmpty
            && photoStatusDict.allSatisfy { photoID, status in
                switch status {
                case .inProgress: return false
                case .success, .failure: return true
                }
            }
    }
    
    func fetchNextPage() {
        Flickr.Rest.searchPublisher(filter: filter, page: nextPage)
            .result()
            .map { Section(page: self.nextPage, photosResult: $0) }
            .receive(on: DispatchQueue.main)
            .sink { self.sections.append($0) }
            .store(in: &subscribers)
    }
    
}
