//
//  Flickr.PhotosResponse.Page+ViewModel.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation

extension Flickr.PhotosResponse.Page {
    
    var pageOfPagesString: String {
        "Page \(page) of \(pageCount)"
    }
    
    var photosOfTotalString: String {
        "\(photos.count) of \(total) photos"
    }
    
}
