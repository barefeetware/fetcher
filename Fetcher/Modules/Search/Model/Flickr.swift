//
//  Flickr.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation

/// Root domain name for all Flickr API types.
struct Flickr {
    // FIXME: Remove the API key from the codebase and repo.
    static let apiKey = "e6bcc7b5c714aaf18a890e6650a4a4d5"
}
