//
//  Flickr.PhotosResponse.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation

extension Flickr {
    struct PhotosResponse {
        let page: Page
        let status: String

        struct Page {
            let page: Int
            let pageCount: Int
            let perPageCount: Int
            let total: String
            let photos: [Photo]
        }
    }
}

extension Flickr.PhotosResponse: Decodable {
    enum CodingKeys: String, CodingKey {
        case page = "photos"
        case status = "stat"
    }
}

extension Flickr.PhotosResponse.Page: Decodable {
    enum CodingKeys: String, CodingKey {
        case page
        case pageCount = "pages"
        case perPageCount = "perpage"
        case total
        case photos = "photo"
    }
}

extension Flickr.PhotosResponse {
    static let empty: Flickr.PhotosResponse = .init(
        page: Page(
            page: 0,
            pageCount: 0,
            perPageCount: 0,
            total: "0",
            photos: []
        ),
        status: "OK"
    )
}
