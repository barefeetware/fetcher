//
//  Flickr.Rest.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation
import BFWFetch

extension Flickr {
    struct Rest {}
}

extension Flickr.Rest: Fetchable {
    
    static var baseURL: URL { URL(string: "https://api.flickr.com/services/")! }
    
    typealias Fetched = Flickr.PhotosResponse
    
    enum Key: String, FetchKey {
        case apiKey = "api_key"
        case method
        case format
        case noJSONCallback = "nojsoncallback"
        case filter = "text"
        case page
    }
    
}
