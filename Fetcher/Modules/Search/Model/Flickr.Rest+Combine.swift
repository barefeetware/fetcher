//
//  Flickr.Rest+Combine.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import Foundation
import Combine

extension Flickr.Rest {
    static func searchPublisher(
        filter: String,
        page: Int? = nil
    ) -> AnyPublisher<Fetched, Error> {
        // The Flickr API produces an error if the filter text is an empty string, so bypass to return just an empty response.
        guard !filter.isEmpty else {
            return Just(.empty)
                .mapError { _ -> Error in }
                .eraseToAnyPublisher()
        }
        return publisher(
            keyValues: [
                .apiKey: Flickr.apiKey,
                .method: "flickr.photos.search",
                .format: "json",
                .noJSONCallback: true,
                .filter: filter,
                .page: page
            ]
        )
    }
}
