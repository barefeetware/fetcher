//
//  SearchScene.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import SwiftUI

struct SearchScene {
    @StateObject var viewModel = ViewModel()
}

extension SearchScene: View {
    var body: some View {
        ScrollView {
            LazyVGrid(columns: columns, spacing: ViewModel.Grid.spacing) {
                Section(header: searchField) { EmptyView() }
                ForEach(viewModel.sections) { section in
                    switch section.photosResult {
                    case .none:
                        Text("Nil")
                    case .failure(let error):
                        // FIXME: Nicer error display.
                        Text(String(describing: error))
                    case .success(let photosResponse):
                        Section(header: summary(page: photosResponse.page)) {
                            cells(photos: photosResponse.page.photos)
                        }
                    }
                }
                Section(header: nextPageLoadingView) { EmptyView() }
            }
            .padding()
        }
        .onAppear { viewModel.onAppear() }
        .navigationTitle(viewModel.title)
        .navigationBarTitleDisplayMode(.inline)
    }
}

private extension SearchScene {
    
    var searchField: some View {
        // TODO: Search bar: Add x and cancel buttons. Move to navigation bar when active.
        TextField(viewModel.searchPlaceholder, text: $viewModel.filter)
            .textFieldStyle(RoundedBorderTextFieldStyle())
    }
    
    func summary(page: Flickr.PhotosResponse.Page) -> some View {
        HStack(alignment: .top) {
            Text(page.photosOfTotalString)
            Spacer()
            Text(page.pageOfPagesString)
        }
        .font(.headline)
        .padding(.vertical)
    }
    
    var columns: [GridItem] {
        [
            GridItem(
                .adaptive(
                    minimum: ViewModel.Grid.minimum,
                    maximum: ViewModel.Grid.maximum
                ),
                spacing: ViewModel.Grid.spacing)
        ]
    }
    
    func cells(photos: [Photo]) -> some View {
        ForEach(photos) { photo in
            Group {
                switch viewModel.status(photo: photo) {
                case .none, .inProgress:
                    ProgressView()
                case .failure(_):
                    failedCell
                case .success(let image):
                    imageCell(photo: photo, image: image)
                }
            }
            .onAppear { viewModel.onAppear(photo: photo) }
        }
    }
    
    func imageCell(photo: Photo, image: Image) -> some View {
        NavigationLink(
            destination: PhotoView(image: image, text: Text(photo.title)),
            label: {
                image
                    .resizable(resizingMode: .stretch)
                    .aspectRatio(contentMode: .fit)
            }
        )
    }
    
    var failedCell: some View {
        Image(systemName: ViewModel.Symbol.error.rawValue)
            .foregroundColor(.red)
    }
    
    var nextPageLoadingView: some View {
        Group {
            if viewModel.isVisibleNextPageLoadingView {
                HStack(spacing: 8) {
                    ProgressView()
                    Text(viewModel.loadingMessage)
                }
                .padding(.vertical)
                .onAppear { viewModel.onAppearNextPageLoadingView() }
            }
        }
    }
    
}

struct SearchScene_Previews: PreviewProvider {
    static var previews: some View {
        SearchScene()
    }
}
