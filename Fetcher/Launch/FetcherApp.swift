//
//  FetcherApp.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import SwiftUI

@main
struct FetcherApp: App {
    var body: some Scene {
        WindowGroup {
            RootScene()
        }
    }
}
