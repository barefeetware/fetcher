//
//  RootScene.swift
//  Fetcher
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import SwiftUI

struct RootScene {}

extension RootScene: View {
    var body: some View {
        NavigationView {
            SearchScene()
        }
        // Prevent split screen on large devices or rotated landscape:
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct RootScene_Previews: PreviewProvider {
    static var previews: some View {
        RootScene()
    }
}
