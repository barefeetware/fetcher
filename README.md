#  Fetcher
Sample app that fetches photos from the Flickr API, matching the search text

## Frameworks

Fetcher uses the open source `BFWFetch` framework, as a git submodule.
https://bitbucket.org/barefeetware/bfwfetch/

When checking out this project, include the submodules. SourceTree and other GUI's do this automtaically. If using the command line, then checkout as:

```
git clone --recurse-submodules https://bitbucket.org/barefeetware/fetcher.git
```

## Architecture

The app interacts wtih the Flickr API as below:

1. The `SearchScene`'s `TextField` updates its `ViewModel`'s `@Published var filter`.
2. The `SearchScene.ViewModel`'s `onAppear()` subscribes to the `$filter`, using `debounce()` to delay for 0.3 seconds before proceding, so the user can usually finish typing their search terms before the app hits the API.
3. The `ViewModel` passes the output of the `filter` publisher into the `Flickr.Rest.searchPublisher()` using `.flatMap`.
4. `Flickr.Rest` conforms to `BFWFetch`'s `Fetchable` protocol. It provides the required `baseURL`, `Fetched` type (`Flickr.PhotosResponse`) and the fetch `Key`s (`apiKey`, `method`, `format`, `filter = "text"`, `page` etc).
5. The `Flickr.Rest.searchPublisher()` takes the `filter` text from the `TextField` and adds parameter key values for the `.apiKey`, `.format: "json"`, `.method: "flickr.photos.search"` etc. If the `filter` text is an empty string, it avoids calling the API, since that raises an error. 
6. Back in the `ViewModel`, the search response is mapped into a `.result()`, so that the publisher will not have an error and will therefore never complete, but will instead keep observing search input. The error from the publisher is instead embedded in the `Result` of the new publisher.
7. The result is mapped into a `Section` as the single item in an array. The `Section` contains the page number `1` and the `photosResult`, which includes the first page metadata and an array of the meta data of the first page of photos.
8. The subscriber sets the `ViewModel`'s `var sections` to the published value.
9. Since the `ViewModel` is observed by the `SearchScene` and the `sections` is `@Published`, the SwiftUI layout updates when `sections` is updated. 
10. In `SearchScene`, the `LazyVGrid` loops through each section, showing the `cells` for the photos in the `photosResponse`.
11. `cells()` loops through all the photos in the page, showing either a `ProgressView`, `failedCell` or an `imageCell`, depending of the status of the fetching of that photo.
12. When each cell appears, it calls the `SearchScene.ViewModel`'s `fetchImage()` for that photo. `fetchImage()` executes once per photo. It uses the `photo`'s `url` to create a `URLRequest`, which it turns into a `dataPublisher()` and maps that into an `Image` (or an error). It stores the image for each `photo.id` in the  `photoStatusDict`.
13. At the end of the `LazyVGrid` is the `nextPageLoadingView` which shows a next page loading indicator, when the previous page has finished fetching. When the user scrolls to the bottom to see the `nextPageLoadingView`, it fires off the `ViewModel`'s `fetchNextPage()`, which uses the `Flickr.Rest.searchPublisher()` again, but this time with the `page: nextPage` parameter. The photos from the response are appended as another section/page in the `sections`, which causes the SwiftUI scene to add cells from the new section and call `fetchImage()` again for the new photos.
14. The unit tests are just basic, for illustration. The `FetcherTests` target directly includes the tested non view source files. It does not depend on the app target to run, which means the tests run much faster. 
