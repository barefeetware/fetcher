//
//  FetcherTests.swift
//  FetcherTests
//
//  Created by Tom Brodhurst-Hill on 14/4/21.
//

import XCTest

class FetcherTests: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPhotoURL() {
        let url = Photo.basic.url
        XCTAssertEqual(url, URL(string: "https://farm4567.static.flickr.com/server1234/1234_secret1234.jpg"))
    }
    
    func testPageOfPagesString() {
        let page = Flickr.PhotosResponse.Page.basic
        XCTAssertEqual(page.pageOfPagesString, "Page 3 of 45")
    }
    
    func testPhotosOfTotalString() {
        let page = Flickr.PhotosResponse.Page.basic
        XCTAssertEqual(page.photosOfTotalString, "1 of 890 photos")
    }
    
}
